/**********************************************************
                Timer section
*************************************************************/
const second = 1000,
      minute = second * 60,
      hour = minute * 60,
      day = hour * 24;

let countDown = new Date('feb 10, 2020 00:00:00').getTime(),
    x = setInterval(function() {

      let now = new Date().getTime(),
          distance = countDown - now;

      document.getElementById('days').innerText = Math.floor(distance / (day)),
        document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
        document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
        document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);
    }, second)

/********************************************************
                        OWL CAROUSEL JS
*********************************************************/
$(document).ready(function(){
    $("#card-wrap").owlCarousel({
        items: 3,
        autoplay: true,
//        margin: 20,
        loop: true,
//        nav: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: true,
//        navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>']
    });
});



$(document).ready(function(){
    $("#isotope-container").isotope({});
    
    $("#isotope-filters").on("click","button",function(){
        let filtervalue = $(this).attr("data-filter");
//        console.log(filtervalue);
        
        $("#isotope-container").isotope({
            filter:filtervalue
        });
        $("#isotope-filters").find('.active').removeClass('active');
    
    $(this).addClass('active');
})

});

/*********************************************************
                    PORTFOLIO MAGNIFIC POPUP JS
*********************************************************/


$(document).ready(function(){
    $("#portfolio-wrapper").magnificPopup({
        delegate:'a',
        type:'image',
        gallery:{
            enabled:true
        },
        zoom:{
            enabled:true,
            duration:300,
            easing:'ease-in-out',
            opener:function(openerElement){
                return openerElement.is('img') ? openerElement:openerElement.find('img');
            }
        }
    })
});

/*********************************************************
                  Smooth scroll
*********************************************************/

$(function(){
    $("a.smooth-scroll").click(function(event){
        event.preventDefault();
        var section_id = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section_id).offset().top +50
        }, 1250, "easeInOutExpo")
    });
});